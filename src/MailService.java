import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class MailService<T> implements Consumer<Mail<T>> {
    private final Map<String, List<T>> mailBox;
    MailService() {
        mailBox =  new HashMap<String, List<T>>();
    }
    @Override
    public void accept(Mail mailMessage) {
        List<T> messages = mailBox.get(mailMessage.getTo());
        if(messages == null) {
            messages = new LinkedList<T>();
            mailBox.put(mailMessage.getTo(), messages);
        }
        messages.add((T) mailMessage.getContent());
        //System.out.println(mailMessage.getContent());

    }

    public Map<String, List<T>> getMailBox() {
        return mailBox;
    }
}
